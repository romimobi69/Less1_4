FROM debian:9 as build
ENV NGINX_VER=nginx-1.0.5 
RUN apt update && apt install -y debconf libterm-readline-gnu-perl zlib1g zlib1g-dev libpcre3 libpcre3-dev wget gcc make
RUN wget http://nginx.org/download/$NGINX_VER.tar.gz && tar xvfz $NGINX_VER.tar.gz 
RUN cd $NGINX_VER && ./configure && make && make install
 
FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
CMD ["./nginx", "-g", "daemon off;"]
